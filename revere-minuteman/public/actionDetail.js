
var actions = JSON.parse(actionJson);
var currentActionIndex = 0;
var currentDeliveryIndex = 0;
var kkeys = [], konami = "38,38,40,40,37,39,37,39,66,65";
$(document).keydown(function(e) {
  kkeys.push( e.keyCode );
  if ( kkeys.toString().indexOf( konami ) >= 0 ){
    $(document).unbind('keydown',arguments.callee);
    $.getScript('http://www.cornify.com/js/cornify.js',function(){
      cornify_add();
      $(document).keydown(cornify_add);
    });
  }
});
function loadDelivery(actionIndex, deliveryIndex){
    currentActionIndex = actionIndex;
    currentDeliveryIndex = deliveryIndex;
    $('#addMsgTarget').val(actions[actionIndex].deliveries[deliveryIndex].target.channelname);
    $('#addMsgName').val(actions[actionIndex].deliveries[deliveryIndex].target.name);
    $('#addMsgTitle').val(actions[actionIndex].deliveries[deliveryIndex].content.name);
    $('#addMsgWebhook').val(actions[actionIndex].deliveries[deliveryIndex].target.webhookpath);
    $('#addMsgMessage').val(actions[actionIndex].deliveries[deliveryIndex].content.message.replace(/\\r\\n/g, "\r\n"));

    //$('#changeMessage').click(function(){setDelivery(actionIndex, deliveryIndex); });
    $('#newDelivery').val('false');
    $('#actionIndex').val(actionIndex);
    $('#deliveryIndex').val(deliveryIndex);
    $('#deleteActionIndex').val(actionIndex);
    $('#deleteDeliveryIndex').val(deliveryIndex);
    $('#changeMessage').val('Change Message');
    $('#deleteButton').show();
}

function addDelivery(actionIndex){
    $('#addMsgTarget').val('');
    $('#addMsgTitle').val('');
    $('#addMsgName').val('');
    $('#addMsgMessage').val('');
    $('#addMsgWebhook').val('');
    $('#newDelivery').val('true');
    $('#actionIndex').val(actionIndex);
    $('#deleteDeliveryIndex').val(-1);
    $('#changeMessage').val('Add Message');
    $('#deleteButton').hide();
}

function populateDeliveries(){
    $("#actionDetailScrollDiv").empty();

    actions.forEach(function(element, index) {
        $("#actionDetailScrollDiv").append('<div class="actionDetailHeader">' + element.actionphrase + '</div>');
        
        var deliveryHTML = '<div class="actionDetailTrigger">';
        var numElements = element.deliveries.length;
        element.deliveries.forEach(function(delivery, deliveryIndex) {
            var deliveryMessage;
            if(delivery.content.message.length < 60){
                deliveryMessage = delivery.content.message.replace(/\\r\\n/g, " ");
            } else {
                deliveryMessage = delivery.content.message.replace(/\\r\\n/g, " ").substring(0, 60) + '...';
            }

            var deliveryTypeClass;
            if(delivery.target.deliverytype == 'Slack'){
                deliveryTypeClass = 'slackBtn';
            } else {
                deliveryTypeClass = 'emailBtn';
            }

            deliveryHTML += '<div class="actionDetailRow"><div class="actionDetailMsgCol">';
            deliveryHTML += '<button onclick="loadDelivery('+ index + ', ' +  deliveryIndex + ')" id="actionDetailButton' + deliveryIndex + '" type="button" class="button">' + deliveryMessage + '</button></div>'
            deliveryHTML += '<div class="actionDetailDlvryCol ADDeliveryCol"><button onclick="loadDelivery('+ index + ', ' +  deliveryIndex + ')" id="actionDetailButton' + deliveryIndex + '" type="button" class="button ' + deliveryTypeClass + '">';
            deliveryHTML += delivery.target.name;
            deliveryHTML += '</button></div></div>';
        }, this);

        // Add the + new message button
        deliveryHTML += '<div class="actionDetailRow actionDetailLastRow"><div class="actionDetailMsgCol">&nbsp;</div>'
        deliveryHTML += '<div class="actionDetailDlvryCol ADDeliveryCol"><button onclick="addDelivery('+ index + ')" type="button" class="button addDeliveryBtn">';
        deliveryHTML += 'Add Message';
        deliveryHTML += '</button></div></div>';

        deliveryHTML += '</div>';

        $("#actionDetailScrollDiv").append(deliveryHTML);

    }, this);
    $('.actionDetailTrigger button').click(function(){$('#wizard').smartWizard('goToStep', 2);});
};

populateDeliveries();