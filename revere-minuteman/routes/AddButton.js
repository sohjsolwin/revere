var express = require('express');
var router = express.Router();
var request = require('request');

var http = require("http");
var https = require("https");

function randomID() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4();
}

router.route('/')
    // GET trigger
    .get(function (req, res) {
        res.render('AddButton', { });
    })
    // POST trigger
    .post(function (req, res) {
        var triggerID = randomID();
        var clientSecret = randomID();
        var postParams = req.body;

        var newButtonJson = '{"triggerid":"' + triggerID + '","clientsecret":"' + clientSecret + '","triggertype":"Button","name":"' + postParams.buttonName + '","actions":[{"actionphrase":"SINGLE_CLICK","actionid":3,"deliveries":[]},{"actionphrase":"DOUBLE_CLICK","actionid":8,"deliveries":[]},{"actionphrase":"LONG_CLICK","actionid":83,"deliveries":[]}]}';

        var newButton = JSON.parse(newButtonJson);
        request({
            url: "http://localhost:3000/trigger/",
            method: "POST",
            json: true,   // <--Very important!!!
            body: newButton
        }, function (error, response, body){
            console.log(response);
            console.log("NEW TRIGGER ID: " + triggerID);
            res.redirect('/ActionWizard/' + triggerID);
        });
    });


module.exports = router;