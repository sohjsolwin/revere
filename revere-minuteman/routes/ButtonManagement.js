var express = require('express');
var router = express.Router();
var app = express();
var Button = require('../models/button.js');
var passwordHash = require('password-hash');
var session = require('express-session');

var http = require("http");
var https = require("https");

var app = express();

router.route('/')
    // GET trigger by ID
    .get(function (req, res) {
        app.use(session({secret: 'TheBritishAreComing'}));
        var sess;
        sess=req.session;
        if(sess.login) {
            http.get({
                host: 'localhost',
                port: 3000,
                path: '/trigger'
            }, function(response) {
                // Continuously update stream with data
                var body = '';
                response.on('data', function(d) {
                    body += d;
                });
                response.on('end', function() {

                    // Data reception is done, do whatever with it!
                    var buttons = JSON.parse(body);
                    var frontEndButtons = new Array();

                    global.buttons = buttons;

                    buttons.forEach(function(element, index) {
                        var newButton = new Button({triggerid: element.triggerid,
                                                    name: element.name
                                                    });
                        frontEndButtons[index] = newButton;
                    }, this);

                    res.render('ButtonManagement', { buttonsJson : JSON.stringify(frontEndButtons) });
                });
            });
        } else {
            res.redirect('../');
        }
    })
    .post(function (req, res) {
        app.use(session({secret: 'TheBritishAreComing'}));

        var postParams = req.body;
        http.get({
            host: 'localhost',
            port: 3000,
            path: '/auth/' + postParams.password
        }, function(response) {
            // Continuously update stream with data
            var body = '';
            response.on('data', function(d) {
                body += d;
            });
            response.on('end', function() {
                var loginText = JSON.parse(body);
                if(loginText.includes("Success")){
                    var sess;
                    sess=req.session;
                    
                    sess.login = "Revere";
                    http.get({
                        host: 'localhost',
                        port: 3000,
                        path: '/trigger'
                    }, function(response) {
                        // Continuously update stream with data
                        var body = '';
                        response.on('data', function(d) {
                            body += d;
                        });
                        response.on('end', function() {

                            // Data reception is done, do whatever with it!
                            var buttons = JSON.parse(body);
                            var frontEndButtons = new Array();

                            global.buttons = buttons;

                            buttons.forEach(function(element, index) {
                                var newButton = new Button({triggerid: element.triggerid,
                                                            name: element.name
                                                            });
                                frontEndButtons[index] = newButton;
                            }, this);

                            res.render('ButtonManagement', { buttonsJson : JSON.stringify(frontEndButtons) });
                        });
                    });
                } else {
                    req.session.destroy();
                    res.redirect('../');
                }
            });
        });
    });

module.exports = router;
