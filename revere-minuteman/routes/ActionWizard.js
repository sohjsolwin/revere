var express = require('express');
var router = express.Router();
var request = require('request');

var http = require("http");
var https = require("https");

router.route('/')
    // GET trigger
    .get(function (req, res) {
        res.redirect('../');
    })
    // POST trigger
    .post(function (req, res) {
        res.redirect('../');
    });

router.route('/NameChange')
    // GET trigger
    .get(function (req, res) {
        res.redirect('../');
    })
    // POST trigger
    .post(function (req, res) {
        var triggerId = req.query.id;
        var secret = null;
        var postParams = req.body;
        var buttons = null;

        http.get({
            host: 'localhost',
            port: 3000,
            path: '/trigger'
        }, function(response) {
            // Continuously update stream with data
            var body = '';
            response.on('data', function(d) {
                body += d;
            });
            response.on('end', function() {
                buttons = JSON.parse(body);

                buttons.forEach( function (element, index){
                    if(element.triggerid == triggerId){
                        secret = element.clientsecret;
                    }
                });
                if(secret == null){
                    res.redirect('../');
                }

                http.get({
                    host: 'localhost',
                    port: 3000,
                    path: '/trigger/' + triggerId + '?secret=' + secret
                }, function(response) {
                    var body = '';
                    response.on('data', function(d) {
                        body += d;
                    });
                    response.on('end', function() {
                        body = body.replace(/\\'/g,'\\\'');
                        var currButton = JSON.parse(body)[0];
                        currButton.name = postParams.changeName;

                        request({
                            url: "http://localhost:3000/trigger/",
                            method: "POST",
                            json: true,   // <--Very important!!!
                            body: currButton
                        }, function (error, response, body){
                            console.log(response);
                            res.redirect(triggerId);
                        });

                    });
                });
            });
        });

    });

router.route('/Delete')
    // GET trigger
    .get(function (req, res) {
        res.redirect('../');
    })
    // POST trigger
    .post(function (req, res) {
        var triggerId = req.query.id;
        var secret = null;
        var postParams = req.body;
        var buttons = null;

        if(postParams.deleteDeliveryIndex == -1){
            res.redirect(triggerId);
        } else {
            http.get({
                host: 'localhost',
                port: 3000,
                path: '/trigger'
            }, function(response) {
                // Continuously update stream with data
                var body = '';
                response.on('data', function(d) {
                    body += d;
                });
                response.on('end', function() {
                    buttons = JSON.parse(body);

                    buttons.forEach( function (element, index){
                        if(element.triggerid == triggerId){
                            secret = element.clientsecret;
                        }
                    });
                    if(secret == null){
                        res.redirect('../');
                    }

                    http.get({
                        host: 'localhost',
                        port: 3000,
                        path: '/trigger/' + triggerId + '?secret=' + secret
                    }, function(response) {
                        var body = '';
                        response.on('data', function(d) {
                            body += d;
                        });
                        response.on('end', function() {
                            var currButton = JSON.parse(body)[0];
                            var AI = parseInt(postParams.deleteActionIndex);
                            var DI = parseInt(postParams.deleteDeliveryIndex);
                            currButton.actions[AI].deliveries.splice(DI, 1);

                            request({
                                url: "http://localhost:3000/trigger/",
                                method: "POST",
                                json: true,   // <--Very important!!!
                                body: currButton
                            }, function (error, response, body){
                                console.log(response);
                            });

                            res.redirect('' + triggerId);

                        });
                    });
                });
            });
        }
    });

router.route('/:id')
    // GET trigger by ID
    .get(function (req, res) {
        var triggerId = req.params.id;
        var secret = null;

        var buttons = null;

        http.get({
            host: 'localhost',
            port: 3000,
            path: '/trigger'
        }, function(response) {
            // Continuously update stream with data
            var body = '';
            response.on('data', function(d) {
                body += d;
            });
            response.on('end', function() {
                buttons = JSON.parse(body);
                buttons.forEach( function (element, index){
                    if(element.triggerid == triggerId){
                        secret = element.clientsecret;
                    }
                });
                if(secret == null){
                    res.redirect('../');
                }

                http.get({
                    host: 'localhost',
                    port: 3000,
                    path: '/trigger/' + triggerId + '?secret=' + secret
                }, function(response) {
                    // Continuously update stream with data
                    var body = '';
                    response.on('data', function(d) {
                        body += d;
                    });
                    response.on('end', function() {

                        // Data reception is done, do whatever with it!
                        var currButton = JSON.parse(body)[0];

                        if(currButton.length == 0){
                            res.redirect('../');
                        } else {
                            res.render('ActionWizard', { actionsJson : JSON.stringify(currButton.actions).replace(/'/g,'\\\''),
                                                        buttonName: currButton.name,
                                                        triggerId: triggerId,
                                                        triggerIdDelete: triggerId });
                        }
                    });
                });
            });
        });

    })
    // POST trigger by ID
    .post(function (req, res) {
        var triggerId = req.params.id;
        var secret = null;
        var postParams = req.body;
        var buttons = null;

        http.get({
            host: 'localhost',
            port: 3000,
            path: '/trigger'
        }, function(response) {
            // Continuously update stream with data
            var body = '';
            response.on('data', function(d) {
                body += d;
            });
            response.on('end', function() {
                buttons = JSON.parse(body);

                buttons.forEach( function (element, index){
                    if(element.triggerid == triggerId){
                        secret = element.clientsecret;
                    }
                });
                if(secret == null){
                    res.redirect('../');
                }

                http.get({
                    host: 'localhost',
                    port: 3000,
                    path: '/trigger/' + triggerId + '?secret=' + secret
                }, function(response) {
                    var body = '';
                    response.on('data', function(d) {
                        body += d;
                    });
                    response.on('end', function() {
                        body = body.replace(/\\'/g,'\\\'');
                        var currButton = JSON.parse(body)[0];
                        var AI = parseInt(postParams.actionIndex);
                        var DI = parseInt(postParams.deliveryIndex);
                        if(postParams.newDelivery == "true"){
                            DI = currButton.actions[AI].deliveries.length;
                            currButton.actions[AI].deliveries[DI] = JSON.parse('{"content": {"message": "", "ishtml": false, "deliverytype": "Slack", "uploadtodropbox": false, "contentid": 98, "name": "", "payload": "<base64encoded string goes here>" }, "deliverytype": "Slack", "target": {"deliverytype":"Slack", "channelname": "", "name": "", "webhookpath": "", "targetid": 8, "botname": "Revere Bot" }, "deliveryid": 6 }');
                        }
                        currButton.actions[AI].deliveries[DI].target.channelname = postParams.addMsgTarget;
                        currButton.actions[AI].deliveries[DI].target.name = postParams.addMsgName;
                        currButton.actions[AI].deliveries[DI].target.webhookpath = postParams.addMsgWebhook;
                        currButton.actions[AI].deliveries[DI].content.name = postParams.addMsgTitle;
                        currButton.actions[AI].deliveries[DI].content.message = postParams.addMsgMessage;

                        request({
                            url: "http://localhost:3000/trigger/",
                            method: "POST",
                            json: true,   // <--Very important!!!
                            body: currButton
                        }, function (error, response, body){
                            console.log(response);
                        });

                        res.render('ActionWizard', { actionsJson : JSON.stringify(currButton.actions).replace(/'/g,'\\\''),
                                                        buttonName: currButton.name,
                                                        triggerId: triggerId,
                                                        triggerIdDelete: triggerId });
                    });
                });
            });
        });
    })
    ;

module.exports = router;
