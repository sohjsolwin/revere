package slack

import (
	"fmt"

	enums "bitbucket.org/sohjsolwin/revere/revere-lantern/enums"
	interfaces "bitbucket.org/sohjsolwin/revere/revere-lantern/interfaces"
	webhook "github.com/ashwanthkumar/slack-go-webhook"
)

/*
DIRECTIONS:
go to your Slack team and navigate to the Custom Integrations > Incoming Webhooks page.
Select [Add configuration] from the left hand side of the page.
Select the channel the Delivery will post to
Once created, you will be provided with a Webhook URL and several other configuration options.
Update the options as desired, specifically the {Customize Name} section and possibly the {Customize Icon} section.

//https://hooks.slack.com/services/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
*/

type SlackDelivery struct {
	DeliveryID   int64                 `json:"deliveryid"`
	DeliveryType string                `json:"deliverytype"`
	Target       *SlackDeliveryTarget  `json:"target"`
	Content      *SlackDeliveryContent `json:"contant"`
}

type SlackDeliveryTarget struct {
	interfaces.DeliveryTarget
	WebhookPath string `json:"webhookpath"`
	BotName     string `json:"botname"`
	ChannelName string `json:"channelname"`
}

type SlackDeliveryContent struct {
	interfaces.DeliveryContent
	IsHTML                bool   `json:"ishtml"`
	Payload               []byte `json:"payload"`
	ShouldUploadToDropBox bool   `json:"uploadtodropbox"` //Not intended to be used now
}

func New(deliveryID int64, target *SlackDeliveryTarget, content *SlackDeliveryContent) *SlackDelivery {
	delivery := &SlackDelivery{
		deliveryID,
		string(enums.DeliveryTypeSlack),
		target,
		content,
	}

	return delivery
}

func (s SlackDelivery) Deliver(payload *interfaces.Payload) error {
	//TODO: Add in the stuff that will deliver this message to Slack in here.
	attachment1 := webhook.Attachment{}
	attachment1.
		AddField(webhook.Field{Title: "Author", Value: s.Target.BotName}).
		AddField(webhook.Field{Title: "Subject", Value: payload.Title}).
		AddField(webhook.Field{Title: "Action Message", Value: s.Content.Message}).
		AddField(webhook.Field{Title: "Content Message", Value: payload.Content}).
		AddField(webhook.Field{Title: "Extra Details", Value: fmt.Sprintf(
			"This message was processed through the %s action of the %s [%s] trigger.",
			payload.ActionPhrase, payload.TriggerName, payload.TriggerType)})

	//(text, username, imageOrIcon, channel string, attachments []Attachment)
	slackPayload := webhook.Payload("See below for alert information",
		s.Target.BotName,
		"",
		s.Target.ChannelName,
		[]webhook.Attachment{attachment1})

	errs := webhook.Send(s.Target.WebhookPath, "", slackPayload)
	if errs != nil && len(errs) > 0 {
		return errs[0] //just return first error for now. This is bad, but i don't care right now.
	}
	return nil //fmt.Errorf("Reached Delivery endpoint for payload %s and target %s", payload, s)
}

func CreateSlackTrigger(jsonMap map[string]interface{}) (interfaces.Trigger, error) {
	trigger := interfaces.Trigger{}
	var err error
	for key, value := range jsonMap {
		switch key {
		case "actions":
			actions := value.([]interface{})
			trigger.Actions, err = createSlackActions(actions)
			break
		case "clientsecret":
			trigger.ClientSecret = value.(string)
			break
		case "name":
			trigger.TriggerName = value.(string)
			break
		case "triggerid":
			trigger.TriggerID = value.(string)
			break
		case "triggertype":
			trigger.TriggerType = value.(string)
			break
		default:
			err = fmt.Errorf("the key provided in this trigger map is invalid: [%s]", key)
		}
	}

	return trigger, err
}

func createSlackActions(jsonMap []interface{}) ([]interfaces.Action, error) {
	actions := make([]interfaces.Action, len(jsonMap))
	var err error
	for i, value := range jsonMap {
		action := value.(map[string]interface{})
		actions[i], err = createSlackAction(action)
	}

	return actions, err
}

func createSlackAction(jsonMap map[string]interface{}) (interfaces.Action, error) {
	action := interfaces.Action{}
	var err error
	for key, value := range jsonMap {
		switch key {
		case "deliveries":
			deliveries := value.([]interface{})
			action.Deliveries, err = createSlackDeliveries(deliveries)
			break
		case "actionid":
			action.ActionID = int64(value.(float64))
			break
		case "actionphrase":
			action.ActionPhrase = value.(string)
			break
		default:
			err = fmt.Errorf("the key provided in this action map is invalid: [%s]", key)
		}
	}

	return action, err
}

func createSlackDeliveries(jsonMap []interface{}) ([]interfaces.DeliverySender, error) {
	deliveries := make([]interfaces.DeliverySender, len(jsonMap))
	var err error
	for i, value := range jsonMap {
		delivery := value.(map[string]interface{})
		deliveries[i], err = createSlackDelivery(delivery)
	}

	return deliveries, err
}

func createSlackDelivery(jsonMap map[string]interface{}) (interfaces.DeliverySender, error) {
	delivery := SlackDelivery{}
	var err error
	for key, value := range jsonMap {
		switch key {
		case "content":
			content := value.(map[string]interface{})
			var c SlackDeliveryContent
			c, err = createSlackDeliveryContent(content)
			delivery.Content = &c
			break
		case "target":
			target := value.(map[string]interface{})
			var t SlackDeliveryTarget
			t, err = createSlackDeliveryTarget(target)
			delivery.Target = &t
			break
		case "deliveryid":
			delivery.DeliveryID = int64(value.(float64))
			break
		case "deliverytype":
			delivery.DeliveryType = value.(string)
			break
		default:
			err = fmt.Errorf("the key provided in this delivery map is invalid: [%s]", key)
		}
	}

	return delivery, err
}

func createSlackDeliveryContent(jsonMap map[string]interface{}) (SlackDeliveryContent, error) {
	content := SlackDeliveryContent{}
	var err error
	for key, value := range jsonMap {
		switch key {
		case "contentid":
			content.ContentID = int64(value.(float64))
			break
		case "deliverytype":
			content.DeliveryType = value.(string)
			break
		case "ishtml":
			content.IsHTML = value.(bool)
			break
		case "message":
			content.Message = value.(string)
			break
		case "name":
			content.Name = value.(string)
			break
		case "payload":
			content.Payload = []byte(value.(string))
			break
		case "uploadtodropbox":
			content.ShouldUploadToDropBox = value.(bool)
			break
		default:
			err = fmt.Errorf("the key provided in this delivery content map is invalid: [%s]", key)
		}
	}

	return content, err
}

func createSlackDeliveryTarget(jsonMap map[string]interface{}) (SlackDeliveryTarget, error) {
	target := SlackDeliveryTarget{}
	var err error
	for key, value := range jsonMap {
		switch key {
		case "targetid":
			target.TargetID = int64(value.(float64))
			break
		case "deliverytype":
			target.DeliveryType = value.(string)
			break
		case "botname":
			target.BotName = value.(string)
			break
		case "channelname":
			target.ChannelName = value.(string)
			break
		case "name":
			target.Name = value.(string)
			break
		case "webhookpath":
			target.WebhookPath = value.(string)
			break
		default:
			err = fmt.Errorf("the key provided in this delivery target map is invalid: [%s]", key)
		}
	}

	return target, err
}
