package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/sohjsolwin/revere/revere-lantern/clients/slack"
	"bitbucket.org/sohjsolwin/revere/revere-lantern/enums"
	"bitbucket.org/sohjsolwin/revere/revere-lantern/interfaces"
	"github.com/revel/revel"
)

type Trigger struct {
	*revel.Controller
}

type PostResult struct {
	Status       int                `json:"status"`
	Error        string             `json:"error"`
	Content      interfaces.Payload `json:"content"`
	HeaderValues []string           `json:"headers"`
	Trigger      interfaces.Trigger `json:"trigger"`
	TriggerMap   interface{}        `json:"triggermap"`
}

func (c Trigger) Fire() revel.Result {
	//actiontype := c.Request.Header.Get("revel-action-type")
	//triggerid := c.Request.Header.Get("revel-trigger-id")
	//triggerauth := c.Request.Header.Get("revel-trigger-auth")
	debug := c.Request.Header.Get("revere-debug")

	error := ""
	content := interfaces.Payload{}
	trigger := interfaces.Trigger{}
	var triggermap interface{}

	err := json.NewDecoder(c.Request.Body).Decode(&content)
	if err == nil {

		// To query for trigger information:
		// To get trigger by ID: ~/trigger/:id
		// eg: http://localhost:3000/trigger/743-alpha?secret=secretphrasegoeshere
		// err = getJson(fmt.Sprintf("http://localhost:3000/trigger/%s?secret=%s",
		// 	content.TriggerID, content.TriggerAuth), &trigger)
		triggermap, err = getJsonMap(fmt.Sprintf("http://localhost:3000/trigger/%s?secret=%s",
			content.TriggerID, content.TriggerAuth))
		if err == nil {
			//return c.RenderJson(triggermap)
			trig := triggermap.([]interface{})
			if len(trig) > 0 {
				trigger, err = slack.CreateSlackTrigger(trig[0].(map[string]interface{}))
				if err == nil {
					if trigger.TriggerName == content.TriggerName &&
						trigger.TriggerType == content.TriggerType &&
						trigger.TriggerID == content.TriggerID &&
						trigger.ClientSecret == content.TriggerAuth {
						for _, action := range trigger.Actions {
							if action.ActionPhrase == content.ActionPhrase {
								err = action.DeliverAll(&content)
								break
							}
						}
						c.Response.Status = 200
					} else {
						c.Response.Status = 422
					}
				}
			} else {
				//Invalid request.
				c.Response.Status = 422
			}

		}
	}

	//result.HeaderValues = []string{
	//	fmt.Sprintf("triggerid=%s", triggerid),
	//	fmt.Sprintf("actiontype=%s", actiontype),
	//	fmt.Sprintf("triggerauth=%s", triggerauth),
	//}

	if err != nil {
		c.Response.Status = 500
		error = err.Error()
	}

	result := PostResult{Status: c.Response.Status, Error: error,
		Content: content, Trigger: trigger, TriggerMap: triggermap}
	//return c.RenderJson(content)
	if debug == "true" {
		return c.RenderJson(result)
	}
	return c.RenderText("Request Received: Response Code: %d", c.Response.Status)

}

func (c Trigger) Slack() revel.Result {
	slack := slack.New(1, &slack.SlackDeliveryTarget{
		DeliveryTarget: interfaces.DeliveryTarget{
			Name:         "Hack Channel",
			TargetID:     2,
			DeliveryType: string(enums.DeliveryTypeSlack),
		},
		WebhookPath: "https://hooks.slack.com/services/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",
		BotName:     "Lantern-bot",
		ChannelName: "spots-hack",
	}, &slack.SlackDeliveryContent{
		IsHTML:                false,
		Payload:               []byte{},
		ShouldUploadToDropBox: false,
		DeliveryContent: interfaces.DeliveryContent{
			ContentID:    3,
			DeliveryType: string(enums.DeliveryTypeSlack),
			Message:      "This is a test message",
			Name:         "Slack Hack Message",
		},
	})

	payload := &interfaces.Payload{
		BinaryPayload: []byte{},
		Content:       "This is a Slack Hack Message",
		Title:         "Slack Hack",
		TriggerID:     "1",
		TriggerName:   "Slack Hack Trigger",
		TriggerType:   string(enums.TriggerTypeSlack),
		ActionPhrase:  "FALSE-CLICK",
	}
	slack.Deliver(payload)
	return c.Render()
}

func getJson(url string, target interface{}) error {
	r, err := http.Get(url)
	if err != nil {
		return err
	}
	defer r.Body.Close()

	return json.NewDecoder(r.Body).Decode(target)
}

func getJsonMap(url string) (interface{}, error) {
	r, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)

	if err != nil {
		return nil, err
	}

	var target interface{}
	err = json.Unmarshal(body, &target)

	return target, err
}
