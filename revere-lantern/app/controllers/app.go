package controllers

import (
	"bitbucket.org/sohjsolwin/revere/revere-lantern/clients/slack"
	"bitbucket.org/sohjsolwin/revere/revere-lantern/enums"
	"bitbucket.org/sohjsolwin/revere/revere-lantern/interfaces"
	"github.com/revel/revel"
)

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	return c.Render()
}

func (c App) Slack() revel.Result {
	slack := slack.New(1, &slack.SlackDeliveryTarget{
		DeliveryTarget: interfaces.DeliveryTarget{
			Name:         "Hack Channel",
			TargetID:     2,
			DeliveryType: string(enums.DeliveryTypeSlack),
		},
		WebhookPath: "https://hooks.slack.com/services/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",
		BotName:     "Lantern-bot",
		ChannelName: "spots-hack",
	}, &slack.SlackDeliveryContent{
		IsHTML:                false,
		Payload:               []byte{},
		ShouldUploadToDropBox: false,
		DeliveryContent: interfaces.DeliveryContent{
			ContentID:    3,
			DeliveryType: string(enums.DeliveryTypeSlack),
			Message:      "This is a test message",
			Name:         "Slack Hack Message",
		},
	})

	payload := &interfaces.Payload{
		BinaryPayload: []byte{},
		Content:       "This is a Slack Hack Message",
		Title:         "Slack Hack",
		TriggerID:     "1",
		TriggerName:   "Slack Hack Trigger",
		TriggerType:   string(enums.TriggerTypeSlack),
		ActionPhrase:  "FALSE-CLICK",
	}
	slack.Deliver(payload)
	return c.Render()
}
