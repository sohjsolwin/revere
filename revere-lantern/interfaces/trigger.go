package interfaces

type Trigger struct {
	TriggerName  string   `json:"name"`
	TriggerType  string   `json:"triggertype"`
	TriggerID    string   `json:"triggerid"`
	ClientSecret string   `json:"clientsecret"`
	Actions      []Action `json:"actions"`
}
