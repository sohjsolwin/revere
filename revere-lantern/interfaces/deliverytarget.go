package interfaces

type DeliveryTarget struct {
	DeliveryType string `json:"deliverytype"`
	Name         string `json:"name"`
	TargetID     int64  `json:"targetid"`
}
