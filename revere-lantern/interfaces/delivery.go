package interfaces

type Delivery struct {
	DeliveryID   int64           `json:"deliveryid"`
	DeliveryType string          `json:"deliverytype"`
	Target       DeliveryTarget  `json:"target"`
	Content      DeliveryContent `json:"content"`
}

type DeliverySender interface {
	Deliver(payload *Payload) error
}
