package interfaces

type Action struct {
	ActionID     int64            `json:"actionid"`
	ActionPhrase string           `json:"actionphrase"`
	Deliveries   []DeliverySender `json:"deliveries"`
}

func (a *Action) DeliverAll(payload *Payload) error {
	for _, delivery := range a.Deliveries {
		//Call the Deliver method for each DeliverySender object
		err := delivery.Deliver(payload)
		if err != nil {
			return err
		} // else {
		// 	return fmt.Errorf("Reached DeliveryAll for delivery: %s", delivery)
		// }
	}
	return nil
}
