package interfaces

type DeliveryContent struct {
	DeliveryType string `json:"deliverytype"`
	Message      string `json:"message"`
	Name         string `json:"name"`
	ContentID    int64  `json:"contentid"`
}
