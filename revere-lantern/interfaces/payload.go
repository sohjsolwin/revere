package interfaces

type Payload struct {
	BinaryPayload []byte `json:"binarypayload"`
	Content       string `json:"content"`
	Title         string `json:"title"`
	TriggerID     string `json:"triggerid"`
	TriggerName   string `json:"triggername"`
	TriggerType   string `json:"triggertype"`
	ActionPhrase  string `json:"actionphrase"`
	TriggerAuth   string `json:"triggerauth"`
}

//TODO: Implement later.
func (p Payload) GetTokenValue(token string) {}
