package enums

type triggerType string

type deliveryType string

const (
	// TriggerTypeWeb is a trigger type representing Web trigger
	TriggerTypeWeb triggerType = "Web"
	// TriggerTypeButton is a trigger type representing Button trigger, Ex: Amazon button
	TriggerTypeButton triggerType = "Button"
	// TriggerTypeSlack is a trigger type representing Slack trigger
	TriggerTypeSlack triggerType = "Slack"
)

//TriggerType provides access to the TriggerType enum values
type TriggerType interface {
	Value() triggerType
}

func (t triggerType) Value() triggerType {
	return t
}

const (
	// DeliveryTypeEmail is a delivery type representing email message
	DeliveryTypeEmail deliveryType = "Email"
	// DeliveryTypeSlack is a delivery representing a slack message
	DeliveryTypeSlack deliveryType = "Slack"
	// DeliveryTypeSMS is a delivery representing a text message
	DeliveryTypeSMS deliveryType = "SMS"
	// DeliveryTypeOPISDelivery is a delivery representing a delivery via OPIS delivery methods
	DeliveryTypeOPISDelivery deliveryType = "OPIS"
)

// DeliveryType provides access to the DeliveryType enum values
type DeliveryType interface {
	Value() deliveryType
}

func (d deliveryType) Value() deliveryType {
	return d
}
