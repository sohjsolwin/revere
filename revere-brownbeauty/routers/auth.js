/********************************************************************************
* auth.js
*  Modular router handler for Authentication
*********************************************************************************/

var express = require('express');
var config = require('config');
var router = express.Router();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('auth.js: ', new Date());
    next();
});

router.route('/:token')
	.get(function (req, res) {
		// todo: should this be in the POST instead of GET?
		var passwordParam = req.params.token; 

		if (config.has('Authentication.LoginPhrase')) {
			var passphrase = config.get('Authentication.LoginPhrase');
			if (passphrase === passwordParam) {
				res.json('Successfully authenticated with provided password: ' + passwordParam);
			} else {
				res.json('Failed to authenticate with provided password: ' + passwordParam);
			}
		} else {
			res.send('ERROR: Authentication.LoginPhrase config value not defined.');
		}
	})
	.post(function (req, res) {
		res.json('POST auth');
	});

module.exports = router;