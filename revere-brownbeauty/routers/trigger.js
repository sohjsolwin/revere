/********************************************************************************
* trigger.js
*  Modular router handler for Trigger objects
*********************************************************************************/

var express = require('express');
var config = require('config');
var aws = require('./../database/aws');
var router = express.Router();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('trigger.js: ', new Date());
    next();
});

var tableName = config.get('Database.Tables.Trigger');

// Handle Trigger requests
router.route('/')
    // GET all triggers
    .get(function (req, res) {
        console.log('trigger.js: ', 'Getting all Triggers');

        var params = {
            TableName: tableName,
        };

        aws.scanDynamoDb(params, function (results) {
            res.json(results);
        });

    })
    // POST new trigger
    .post(function (req, res) {
        console.log('trigger.js: ', 'Posting new Trigger');

        var params = {
            TableName: tableName,
            Item: req.body
        };

        aws.writeToDynamoDb(params, function (results) {
            res.json(results);
        });
    });

router.route('/:id')
    // GET trigger by ID
    .get(function (req, res) {
        var triggerId = req.params.id;
        var secret = req.query.secret;

        if (secret != undefined) {
            console.log('trigger.js: ', 'Getting Trigger with triggerid: [' + triggerId + '] and clientsecret: [' + secret + ']');

            var params = {
                TableName: tableName,
                // select where the triggerid & clientsecret match for client validation
                KeyConditionExpression: "#tId = :triggerId",
                FilterExpression: "#sec = :secret",
                ExpressionAttributeNames: {
                    "#tId": "triggerid",
                    "#sec": "clientsecret"
                },
                ExpressionAttributeValues: {
                    ":triggerId": triggerId,
                    ":secret": secret
                }
            };

            aws.queryDynamoDb(params, function (results) {
                res.json(results);
            });
        } else {
            console.log('trigger.js: ', 'ERROR: Cannot get Trigger [' + triggerId + '] --- No client secret provided.');
            res.status(422).send('ERROR: Client must provide a client secret in order to retreive Trigger [' + triggerId + '].');
        }
    });

router.route('/:id/delete')
    // DELETE trigger by ID
    .get(function (req, res) {
        var triggerId = req.params.id;
        var secret = req.query.secret;

        if (secret != undefined) {
            console.log('trigger.js: ', 'Deleting Trigger with triggerid: [' + triggerId + '] and clientsecret: [' + secret + ']');

            var params = {
                TableName: tableName,
                // select where the triggerid & clientsecret match for client validation
                KeyConditionExpression: "#tId = :triggerId",
                FilterExpression: "#sec = :secret",
                ExpressionAttributeNames: {
                    "#tId": "triggerid",
                    "#sec": "clientsecret"
                },
                ExpressionAttributeValues: {
                    ":triggerId": triggerId,
                    ":secret": secret
                }
            };
            // First, query to make sure the trigger + secret is valid
            aws.queryDynamoDb(params, function (results) {
                if (results.length == 1) {
                    var deleteParams = {
                        TableName: tableName,
                        Key: {
                            "triggerid": triggerId
                        }
                    };

                    // Perform deletion of trigger by ID
                    aws.deleteFromDynamoDb(deleteParams, function (result) {
                        res.send(result);
                    });
                }
            });
        } else {
            console.log('trigger.js: ', 'ERROR: Cannot delete Trigger [' + triggerId + '] --- No client secret provided.');
            res.status(422).send('ERROR: Client must provide a client secret in order to delete Trigger [' + triggerId + '].');
        }
    });


module.exports = router;