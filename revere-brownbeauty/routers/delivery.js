/********************************************************************************
* delivery.js
*  Modular router handler for Delivery objects
*********************************************************************************/

// todo: should delivery, delivery target, and delivery content all be in one endpoint?

var config = require('config');
var express = require('express');
var router = express.Router();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('delivery.js: ', new Date());
    next();
});

var tableName = config.get('Database.Tables.Delivery');

// Handle Delivery requests
router.route('/')
    // GET all deliveries
    .get(function (req, res) {
        console.log('delivery.js: ', 'Getting all Deliveries');

        var params = {
            TableName: tableName,
        };

        aws.scanDynamoDb(params, function(results) {
            res.json(results);
        });
        
    })
    // POST new delivery (should be same structure as the GET all)
    .post(function (req, res) {
        console.log('delivery.js: ', 'Posting new Delivery');
        
        var id = req.body.id;
        var name = req.body.name;

        res.json({ success: 'true', id: id, name: name });
    });

router.route('/:id')
    // GET delivery by ID (need some form of authentication)
    .get(function (req, res) {
        var id = req.params.id;
        console.log('delivery.js: ', 'Getting Delivery with ID: ', id);

        var params = {
            TableName: tableName,
            KeyConditionExpression: "#id = :queryId",
            ExpressionAttributeNames: {
                "#id": "DeliveryId"
            },
            ExpressionAttributeValues: {
                ":queryId": id
            }
        };

        aws.queryDynamoDb(params, function(results) {
            res.json(results);
        });
    });


module.exports = router;