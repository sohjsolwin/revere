/********************************************************************************
* action.js
*  Modular router handler for Action objects
*********************************************************************************/

var config = require('config');
var express = require('express');
var aws = require('./../database/aws');
var router = express.Router();

// middleware that is specific to this router
router.use(function timeLog(req, res, next) {
    console.log('action.js: ', new Date());
    next();
});

var tableName = config.get('Database.Tables.Delivery');

// Handle Action requests
router.route('/')
    // GET all actions
    .get(function (req, res) {
        console.log('action.js: ', 'Getting all Actions');

        var params = {
            TableName: tableName,
        };

        aws.scanDynamoDb(params, function(results) {
            res.json(results);
        });
        
    })
    // POST new action
    .post(function (req, res) {
        console.log('action.js: ', 'Posting new Action');
        
        var id = req.body.id;
        var name = req.body.name;

        res.json({ success: 'true', id: id, name: name });
    });

router.route('/:id')
    // GET delivery by ID
    .get(function (req, res) {
        var id = req.params.id;
        console.log('action.js: ', 'Getting Action with ID: ', id);

        var params = {
            TableName: tableName,
            KeyConditionExpression: "#id = :queryId",
            ExpressionAttributeNames: {
                "#id": "ActionId"
            },
            ExpressionAttributeValues: {
                ":queryId": id
            }
        };

        aws.queryDynamoDb(params, function(results) {
            res.json(results);
        });
    });


module.exports = router;