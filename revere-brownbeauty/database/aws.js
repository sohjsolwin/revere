/********************************************************************************
* aws.js
*  Modular component for interacting with Amazon AWS DynamoDB
*********************************************************************************/

var AWS = require('aws-sdk');
var config = require('config');
// We'll keep our AWS AccessKeyId & SecretAccessKey in a file that will not be checked into source control
var secret = require('./../config/secret.json');

// Configure AWS DynamoDB connection
AWS.config.update({
    region: config.get('Database.Connection.Region'),
    endpoint: config.get('Database.Connection.Endpoint'),
    accessKeyId: secret.AWS.AccessKeyId,
    secretAccessKey: secret.AWS.SecretAccessKey,
});

// Export necessary functions & properties
module.exports = {
    docClient: new AWS.DynamoDB.DocumentClient(),

    // External function to query DynamoDB
    queryDynamoDb: function (params, callback) {
        this.docClient.query(params, function (err, data) {
            if (err) {
                console.error("Unable to query. Error:", JSON.stringify(err, null, 2));
            } else {
                console.log("Query succeeded.");
                console.log("# of results: [" + data.Items.length + "]");
                data.Items.forEach(function (item) {
                    console.info("item - ");
                    console.dir(item);
                });

                callback(data.Items);
            }
        });
    },

    // External function used to scan DynamoDB
    scanDynamoDb: function (params, callback) {
        this.docClient.scan(params, function (err, data) {
            if (err) {
                console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                // print all 
                console.log("Scan succeeded.");
                console.log("# of results: [" + data.Items.length + "]");
                data.Items.forEach(function (item) {
                    console.info("item - ");
                    console.dir(item);
                });

                //continue scanning if we have more 
                if (typeof data.LastEvaluatedKey != "undefined") {
                    console.log("Scanning for more...");
                    params.ExclusiveStartKey = data.LastEvaluatedKey;
                    this.docClient.scan(params, onScan);
                }

                callback(data.Items);
            }
        });
    },

    // External function used to write to DynamoDB
    writeToDynamoDb: function (params, callback) {
        this.docClient.put(params, function (err, data) {
            if (err) {
                console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                console.log("Added item:", JSON.stringify(params.Item.triggerid, null, 2));
            }
            callback(params.Item);
        });
    },

    // External function used to delete from DynamoDb
    deleteFromDynamoDb: function (params, callback) {
        var status = false;
        this.docClient.delete(params, function (err, data) {
            if (err) {
                console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
            } else {
                status = true;
                console.log("Item deleted:", JSON.stringify(params.Key.triggerid, null, 2));
            }
            callback(status);
        });
    }
};

// Internal function to scan DynamoDB
// function onScan(err, data) {
//     if (err) {
//         console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
//     } else {
//         // print all the movies
//         console.log("Scan succeeded.");
//         //data.Items.forEach(function(data) {
//         console.log("# of results: [" + data.Items.length + "]");
//         //});

//         // continue scanning if we have more movies
//         if (typeof data.LastEvaluatedKey != "undefined") {
//             console.log("Scanning for more...");
//             params.ExclusiveStartKey = data.LastEvaluatedKey;
//             docClient.scan(params, onScan);
//         }
//     }
// }



