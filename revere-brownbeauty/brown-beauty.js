/********************************************************************************
* brown-beauty.js
*  Main entry point for Revere Data Service using 
*  Uses NodeJS + ExpressJS
*********************************************************************************/

var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

// Routers
var triggerRouter = require('./routers/trigger');
var deliveryRouter = require('./routers/delivery');
var authRouter = require('./routers/auth');
var actionRouter = require('./routers/action');

var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// Home Page
app.get('/', function (req, res) {
	res.sendFile(process.cwd() + '/content/bb.jpg');
});

// Handle authentication requests
app.use('/auth', authRouter);

// Handle Trigger requests
app.use('/trigger', triggerRouter);

// Handle Delivery requests
app.use('/delivery', deliveryRouter);

// Handle Action requests
app.use('/action', actionRouter);



// START - listen for API calls
app.listen(3000, function () {
	console.log('brown-beauty.js listening on port 3000...');
});